<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if (!empty($this->pageDescription)) {
        echo '<meta name="description" content="' . $this->pageDescription . '" />';
    } ?>

    <?php if (!empty($this->pageKeywords)) {
        echo '<meta name="keywords" content="' . $this->pageDescription . '" />';
    } ?>
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
        ],
    ]);
    $navItems = [

        'items' => [
            ['label' => 'Főoldal', 'url' => ['/site/index']],
            ['label' => 'Oldalak', 'url' => ['/page/'], 'visible' => !Yii::$app->user->isGuest],
            ['label' => 'Kapcsolat', 'url' => ['/site/contact']],
            ['label' => 'Bejelentkezés', 'url' => ['/site/login'], 'visible' => Yii::$app->user->isGuest],
            ['label' => 'Regisztráció', 'url' => ['/site/signup'], 'visible' => Yii::$app->user->isGuest],
            ['label' => 'Kijelentkezés', 'url' => ['/site/logout'], 'visible' => !Yii::$app->user->isGuest]
        ]
    ];

    echo Nav::widget($navItems);
    NavBar::end();
    ?>

    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
