<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

// kcfinder options
// http://kcfinder.sunhater.com/install#dynamic
$kcfOptions = array_merge(\iutbay\yii2kcfinder\KCFinder::$kcfDefaultOptions, [
    'uploadURL' => Yii::getAlias('@web') . '/upload',
    'access' => [
        'files' => [
            'upload' => true,
            'delete' => false,
            'copy' => false,
            'move' => false,
            'rename' => false,
        ],
        'dirs' => [
            'create' => true,
            'delete' => false,
            'rename' => false,
        ],
    ],
]);

// Set kcfinder session options
Yii::$app->session->set('KCFINDER', $kcfOptions);
?>
<div class="page-create">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>
    <?= Html::activeHiddenInput($model, 'user_id'); ?>
    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'link') ?>
    <?= $form->field($model, 'meta_description') ?>
    <?= $form->field($model, 'meta_keys') ?>
    <?= $form->field($model, 'body')->widget(\app\components\CKEditor::className(), [
        'options' => [
            'rows' => 6,
            'toolbarGroups' => [
                ['name' => 'undo'],
                ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                ['name' => 'colors'],
                ['name' => 'links', 'groups' => ['links', 'insert']],
                ['name' => 'others', 'groups' => ['others', 'about']],

                ['name' => 'pbckcode']
            ]
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Mentés', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
