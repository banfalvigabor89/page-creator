<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Oldalak';
?>
<div class="pages-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Oldal létrehozása', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'title',
            'link',
            [
                'header' => 'Műveletek',
                'contentOptions' => ['class' => 'text-center actions-cell'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{v} {update} {delete}',
                'buttons' => [
                    'v' => function ($url, $model, $key) {
                        return Html::a(Html::encode("megtekintés"), (Url::toRoute(['page/v', 'url' => $model->link])),
                            ['class' => 'btn btn-success',]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a(Html::encode("szerkesztés"), $url, ['class' => 'btn btn-primary',]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('Törlés', $url, [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Biztos hogy törölni akarja az elemet',
                                'method' => 'post',
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div>