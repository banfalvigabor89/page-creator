<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pages */

$this->title = $model->title;

?>

<div>
    <h1>
        <?php
        echo Html::decode($model->title);
        ?>
    </h1>
    <?php
    echo Html::decode($model->body);
    ?>
</div>



