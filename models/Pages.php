<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $link
 * @property string $meta_description
 * @property string $meta_keys
 * @property string $body
 *
 * @property User $user
 */
class Pages extends ActiveRecord
{

    public function loadDefaultValues($skipIfSet = true)
    {
        parent::loadDefaultValues($skipIfSet);
        $this->user = Yii::$app->getUser();
        $this->user_id = Yii::$app->getUser()->getId();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'link', 'meta_description', 'meta_keys', 'body'], 'required'],
            [['user_id'], 'integer'],
            ['body', 'string'],
            [['title'], 'string', 'max' => 100],
            [['link', 'meta_description', 'meta_keys'], 'string', 'max' => 255],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Azonosító',
            'user_id' => 'Felhasználó azonosító',
            'title' => 'Cím',
            'link' => 'Link',
            'meta_description' => 'Meta Leírás',
            'meta_keys' => 'Meta Kulcsok',
            'body' => 'Törzs',
        ];
    }

    public function beforeSave($event)
    {
        if (parent::beforeSave($event)) {
            $this->user_id = Yii::$app->getUser()->getId();
            return true;
        } else {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'link',
                'slugAttribute' => 'link',
            ],
        ];
    }

}
