<?php

namespace app\models;


use yii2mod\user\models\UserModel;

class User extends UserModel
{

    public function attributeLabels()
    {
        return [
            'username' => 'Your name',
            'email' => 'Your email address',
        ];
    }

    public function getPages(){
        return $this->hasMany(Pages::className(), ['user_id' => 'id']);
    }
}
