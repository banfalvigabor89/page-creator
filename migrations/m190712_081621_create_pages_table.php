<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pages}}`.
 */
class m190712_081621_create_pages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pages}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->string(100)->notNull(),
            'link' => $this->string(255)->notNull(),
            'meta_description' => $this->string(255)->notNull(),
            'meta_keys' => $this->string(255)->notNull(),
            'body' => $this->text()->notNull()
        ]);
        $this->addForeignKey('FK_page_creator', 'pages', 'user_id', 'user', 'id');
        $this->createIndex('IND_UNIQAUE_COMBO', 'pages', ['user_id', 'link'], 1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pages}}');
    }
}
