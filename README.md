


TELEPÍTÉS
------------
-Checkout

-composer install

-adatbázis beállítása

`config/db.php` fájlban valós adatok behelyettesítése
pl.:
   ```php
    return [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=yii2basic',
        'username' => 'root',
        'password' => '1234',
        'charset' => 'utf8',
    ];
   ```

-Adatbázis létrehozása az adatbázis kezelőben

-migráció
a project főmappájában a  
```php yii migrate```
parancs kiadása


-fő mappában ```php yii serve``` parancs kiadása vagy 
apache/ngix virtual host készítése
ezekhez ezen az oldalon találhatóak instrukciók
 
https://www.yiiframework.com/doc/guide/2.0/en/start-installation#recommended-apache-configuration

